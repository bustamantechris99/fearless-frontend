const cookie = await cookieStore.get("jwt_access_payload")
console.log(cookie)
const cookieValue = cookie.value
// const value = JSON.parse(cookieValue)
const value = atob(cookieValue)
const decoded = JSON.parse(value).user.perms
if (decoded.includes("events.add_conference")){
  const conferenceLink = document.getElementById('new-conference')
    conferenceLink.classList.remove('d-none')
}
if (decoded.includes("events.add_location")){
  const locationLink = document.getElementById('new-location')
  locationLink.classList.remove('d-none')
}
