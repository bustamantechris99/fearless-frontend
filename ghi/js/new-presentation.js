function success() {
  return `
<div class="alert alert-success" role="alert">
  Your presentation has been created!
</div>`;
}
window.addEventListener("DOMContentLoaded", async () => {
  const selectTag = document.getElementById("conference");

  const url = "http://localhost:8000/api/conferences/";
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();

    for (let conference of data.conferences) {
      const option = document.createElement("option");
      option.value = conference.href.slice(-2, -1);
      option.innerHTML = conference.name;
      selectTag.appendChild(option);
    }
  }
  const form = document.getElementById("create-presentation-form");
  form.addEventListener("submit", async (event) => {
    event.preventDefault();
    const select = document.getElementById("conference");
    const conference_id = select.options[select.selectedIndex].value;
    console.log(conference_id);
    console.log("need to submit form data");
    const formData = new FormData(form);
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json);
    const presentationUrl = `http://localhost:8000/api/conferences/${conference_id}/presentations/`;
    const fetchConfig = {
      method: "POST",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      form.reset();
      const newPresentation = await response.json();
      console.log(newPresentation);
      form.innerHTML = success();
    }
  });
});
