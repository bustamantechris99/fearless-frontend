window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/states/";
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    const states = data["states"];
    console.log(data);
    const select = document.getElementById("state");
    for (let state of states) {
      const option = document.createElement("option");
      option.value = state.abbreviation;
      option.innerHTML = state.name;
      select.appendChild(option);
    }
    ``;
    const form = document.getElementById("create-location-form");
    form.addEventListener("submit", async (event) => {
      event.preventDefault();
      console.log("need to submit form data");
      const formData = new FormData(form);
      const json = JSON.stringify(Object.fromEntries(formData));
      console.log(json);
      const locationUrl = "http://localhost:8000/api/locations/";
      const fetchConfig = {
        method: "POST",
        body: json,
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        form.reset();
        const newLocation = await response.json();
        console.log(newLocation);
      }
    });
  }
});
