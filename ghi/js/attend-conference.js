function success() {
  return `
<div class="alert alert-success" role="alert">
  Your name has been added to the conference!
</div>`;
}
window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";
  const selectTag = document.getElementById("conference");
  const loader = document.getElementById("loading-conference-spinner");
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    for (let conference of data.conferences) {
      const option = document.createElement("option");
      option.value = conference.href;
      option.innerHTML = conference.name;
      selectTag.appendChild(option);
    }
    selectTag.classList.remove("d-none");
    loader.classList.add("d-none");
  }
  const form = document.getElementById("create-attendee-form");
  form.addEventListener("submit", async (event) => {
    event.preventDefault();
    const formData = new FormData(form);
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json);
    const conference_id = selectTag.options[
      selectTag.selectedIndex
    ].value.slice(-2, -1);
    console.log(conference_id);
    const attendeesURL = `http://localhost:8001/api/conferences/${conference_id}/attendees/`;
    const fetchData = {
      method: "POST",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(attendeesURL, fetchData);
    if (response.ok) {
      const newAttendee = await response.json();
      console.log(newAttendee);
      form.innerHTML = success();
    }
  });
});
