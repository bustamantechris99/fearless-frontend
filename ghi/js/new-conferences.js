window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/locations";
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    const locations = data.locations;
    console.log(data.locations);
    const select = document.getElementById("location");
    for (let location of locations) {
      const option = document.createElement("option");
      option.value = location.href.slice(-2, -1);
      option.innerHTML = location.name;
      select.appendChild(option);
    }
  }
  const form = document.getElementById("create-location-form");
  form.addEventListener("submit", async (event) => {
    event.preventDefault();
    const formData = new FormData(form);
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json);
    const confUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "POST",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(confUrl, fetchConfig);
    if (response.ok) {
      form.reset();
      const newConference = await response.json();
      console.log(newConference);
    }
  });
});
