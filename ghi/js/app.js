function createCard(name, description, pictureUrl, start, finish, location) {
  return `
  <div class="col-4">
    <div class="card my-lg-3 shadow">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        <small class="text-muted">${start} - ${finish}</small>
      </div>

      </div>
    </div>
  </div>
  `;
}
function error(errorText){
  return `
  <div class="alert alert-danger" role="alert">
    ${errorText}
  </div>`
}

window.addEventListener('DOMContentLoaded', async () => {
const url = 'http://localhost:8000/api/conferences/';
try{
  const response = await fetch(url)
  if(!response.ok){
    const row = document.querySelector('.row')
    row.innerHTML += error('Something was wrong with your response')
  }
  else{
    const data = await response.json()
    for (let conference of data.conferences){
      const detailUrl = `http://localhost:8000${conference.href}`
      const detailResponse = await fetch(detailUrl)
      if (detailResponse.ok){
        const details = await detailResponse.json()
        console.log(details)
        const title = details.conference.name
        const description = details.conference.description
        const image = details.conference.location.picture_url
        const start = new Date(details.conference.starts).toLocaleDateString()
        const finish = new Date(details.conference.ends).toLocaleDateString()
        const location = details.conference.location.name
        const html = createCard(title, description, image, start, finish, location)
        console.log(html)
        const row = document.querySelector('.row')
        row.innerHTML += html
      }
      else{
        const row = document.querySelector('.row')
        row.innerHTML += error('Something was wrong with your response')
      }}
    }
  }
catch(e){
  console.log(e)
}

});
