import React, { useState, useEffect } from "react";

function ConferenceForm() {
  const [name, setName] = useState("");
  const nameChanged = (event) => {
    setName(event.target.value);
  };
  const [starts, setStart] = useState("");
  const startChange = (event) => {
    setStart(event.target.value);
  };
  const [ends, setEnd] = useState("");
  const endChange = (event) => {
    setEnd(event.target.value);
  };
  const [description, setDescription] = useState("");
  const descriptionChanged = (event) => {
    setDescription(event.target.value);
  };
  const [max_presentations, setPresentations] = useState(0);
  const presentationsChanged = (event) => {
    console.log(event.target.value);
    setPresentations(event.target.value);
  };
  const [max_attendees, setAttendees] = useState(0);
  const attendeesChanged = (event) => {
    console.log(event.target.value);
    setAttendees(event.target.value);
  };
  const [location, setLocation] = useState();
  const locationChanged = (event) => {
    console.log(event.target.value);
    setLocation(event.target.value);
  };
  const [locations, setLocations] = useState([]);
  const getLocationData = async () => {
    const url = "http://localhost:8000/api/locations";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
      console.log(data.locations);
    }
  };
  const submitForm = async (event) => {
    event.preventDefault();
    const body = {
      name,
      starts,
      ends,
      description,
      max_presentations,
      max_attendees,
      location,
    };
    console.log(body);
    const locationUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
      setName("");
      setStart("");
      setEnd("");
      setDescription("");
      setPresentations(0);
      setAttendees(0);
      setLocation("");
    }
  };
  useEffect(() => {
    getLocationData();
  }, []);
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Conference</h1>
          <form id="create-location-form" onSubmit={submitForm}>
            <div className="form-floating mb-3">
              <input
                placeholder="Name"
                required
                type="text"
                id="name"
                className="form-control"
                name="name"
                value={name}
                onChange={nameChanged}
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Starts"
                required
                type="date"
                id="starts"
                className="form-control"
                name="starts"
                value={starts}
                onChange={startChange}
              />
              <label htmlFor="Starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Ends"
                required
                type="date"
                id="ends"
                className="form-control"
                name="ends"
                value={ends}
                onChange={endChange}
              />
              <label htmlFor="Ends">Ends</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description" className="form-label">
                Description
              </label>
              <textarea
                className="form-control"
                id="description"
                rows="3"
                name="description"
                value={description}
                onChange={descriptionChanged}
              ></textarea>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Max Presentations"
                required
                type="number"
                id="max_presentations"
                className="form-control"
                name="max_presentations"
                value={max_presentations}
                onChange={presentationsChanged}
              />
              <label htmlFor="city">Max Presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Max Attendees"
                required
                type="number"
                id="max_attendees"
                className="form-control"
                name="max_attendees"
                value={max_attendees}
                onChange={attendeesChanged}
              />
              <label htmlFor="city">Max Attendees</label>
            </div>
            <div className="mb-3">
              <select
                required
                id="location"
                className="form-select"
                name="location"
                value={location}
                onChange={locationChanged}
              >
                <option>Choose a location</option>
                {locations.map((location) => {
                  return (
                    <option
                      key={location.href}
                      value={location.href.slice(-2, -1)}
                    >
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default ConferenceForm;
