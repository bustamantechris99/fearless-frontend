import React, { useState, useEffect } from "react";

export default function PresentationForm() {
  const [conferences, setConferences] = useState([]);
  const [conference, setConference] = useState("");
  const handleChangeConference = (event) => {
    const value = event.target.value;
    setConference(value);
  };
  const [presenter_name, setName] = useState("");
  const changeName = (event) => {
    console.log(event.target.value);
    setName(event.target.value);
  };
  const [presenter_email, setEmail] = useState("");
  const changeEmail = (event) => {
    console.log(event.target.value);
    setEmail(event.target.value);
  };
  const [company_name, setCompanyName] = useState("");
  const changeCompanyName = (event) => {
    console.log(event.target.value);
    setCompanyName(event.target.value);
  };
  const [title, setTitle] = useState("");
  const changeTitle = (event) => {
    console.log(event.target.value);
    setTitle(event.target.value);
  };
  const [synopsis, setSynopsis] = useState("");
  const changeSynopsis = (event) => {
    console.log(event.target.value);
    setSynopsis(event.target.value);
  };
  const fetchData = async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);
  const submitForm = async (event) => {
    event.preventDefault();
    const body = {
      presenter_name,
      presenter_email,
      company_name,
      title,
      synopsis,
      conference,
    };
    console.log(body);
    const locationUrl = `http://localhost:8000/${conference}presentations/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      console.log(newPresentation);
      setName("");
      setEmail("");
      setCompanyName("");
      setTitle("");
      setSynopsis("");
    }
  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form id="create-presentation-form" onSubmit={submitForm}>
            <div className="form-floating mb-3">
              <input
                placeholder="Presenter name"
                required
                type="text"
                name="presenter_name"
                id="presenter_name"
                className="form-control"
                value={presenter_name}
                onChange={changeName}
              />
              <label htmlFor="presenter_name">Presenter name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Presenter email"
                required
                type="email"
                name="presenter_email"
                id="presenter_email"
                className="form-control"
                value={presenter_email}
                onChange={changeEmail}
              />
              <label htmlFor="presenter_email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Company name"
                type="text"
                name="company_name"
                id="company_name"
                className="form-control"
                value={company_name}
                onChange={changeCompanyName}
              />
              <label htmlFor="company_name">Company name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Title"
                required
                type="text"
                name="title"
                id="title"
                className="form-control"
                value={title}
                onChange={changeTitle}
              />
              <label htmlFor="title">Title</label>
            </div>
            <div className="mb-3">
              <label htmlFor="synopsis">Synopsis</label>
              <textarea
                className="form-control"
                id="synopsis"
                rows="3"
                name="synopsis"
                value={synopsis}
                onChange={changeSynopsis}
              ></textarea>
            </div>
            <div className="mb-3">
              <select
                required
                name="conference"
                id="conference"
                className="form-select"
                onChange={handleChangeConference}
                value={conference}
              >
                <option>Choose a conference</option>
                {conferences.map((conference) => {
                  return (
                    <option value={conference.href} key={conference.href}>
                      {conference.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
