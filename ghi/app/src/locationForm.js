import React, { useEffect, useState } from "react";

function LocationForm() {
  const [name, setName] = useState("");
  const nameChanged = (event) => {
    setName(event.target.value);
  };
  const [room_count, setRoomCount] = useState(0);
  const countChanged = (event) => {
    setRoomCount(event.target.value);
  };
  const [city, setCity] = useState("");
  const cityChanged = (event) => {
    setCity(event.target.value);
  };
  const [state, setState] = useState("");
  const optionChanged = (event) => {
    setState(event.target.value);
  };

  const [states, setStates] = useState([]);
  const fetchData = async () => {
    const url = "http://localhost:8000/api/states/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setStates(data.states);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const submitForm = async (event) => {
    event.preventDefault();
    const body = {
      name,
      room_count,
      city,
      state,
    };
    console.log(body);
    const locationUrl = "http://localhost:8000/api/locations/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);
      setName("");
      setRoomCount("");
      setCity("");
      setState("");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form id="create-location-form" onSubmit={submitForm}>
            <div className="form-floating mb-3">
              <input
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
                value={name}
                onChange={nameChanged}
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Room count"
                required
                type="number"
                name="room_count"
                id="room_count"
                className="form-control"
                value={room_count}
                onChange={countChanged}
              />
              <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="City"
                required
                type="text"
                name="city"
                id="city"
                className="form-control"
                value={city}
                onChange={cityChanged}
              />
              <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
              <select
                required
                name="state"
                id="state"
                className="form-select"
                value={state}
                onChange={optionChanged}
              >
                <option>Choose a state</option>
                {states.map((state) => {
                  return (
                    <option key={state.abbreviation} value={state.abbreviation}>
                      {state.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default LocationForm;
