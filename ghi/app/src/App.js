import "./App.css";
import Nav from "./Nav";
import React from "react";
import AttendeesList from "./AttendeesList";
import LocationForm from "./locationForm";
import ConferenceForm from "./conferenceForm";
import AttendConferenceForm from "./AttendConference";
import PresentationForm from "./presentationForm";
import MainPage from "./mainPage";
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route
            path="attendees"
            element={<AttendeesList attendees={props.attendees} />}
          />
          <Route path="presentation">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>
      </div>
      {/* <div className='container'><ConferenceForm/></div> */}
      {/* <div className='container'><LocationForm/></div> */}
      {/* <AttendeesList attendees={props.attendees}/> */}
    </BrowserRouter>
  );
}

export default App;
